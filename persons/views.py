from django.shortcuts import render
from rest_framework import generics
from persons.serializers import PersonListSerializer
from persons.models import Person
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404

# Create your views here.


class PersonAPIView(APIView):
        def get(self, request):
                queryset = Person.objects.all()
                serializer = PersonListSerializer(queryset, many=True)
                return Response(serializer.data)

        def post(self, request):
                serializer = PersonListSerializer(data=request.data)
                if(serializer.is_valid()):
                        serializer.save()
                        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PersonDetailView(APIView):
        def get_object(self, id):
                try:
                        return Person.objects.get(id=id)
                except Person.DoesNotExist:
                        raise Http404

        def delete(self, request, id):
                queryset = self.get_object(id)
                queryset.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

        def patch(self, request, id):
                queryset = self.get_object(id)
                serializer = PersonListSerializer(queryset, data=request.data)
                if serializer.is_valid():
                        serializer.save()
                        return Response(serializer.data)


