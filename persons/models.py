from django.db import models


class Person(models.Model):
    full_name = models.CharField(verbose_name='Full name', max_length=64)
    birthday = models.DateField(verbose_name='Birthday')
    birth_place = models.CharField(verbose_name='Birthday place', max_length=64)
    doc_number = models.CharField(verbose_name='Doc number', max_length=64)
    doc_pin_code = models.CharField(verbose_name='Doc pin code', max_length=64)
    create_date = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField('Created By', max_length=64)