from django.contrib import admin
from django.urls import path
from persons.views import *

urlpatterns = [
    path('', PersonAPIView.as_view()),
    path('<int:id>/', PersonDetailView.as_view())
]