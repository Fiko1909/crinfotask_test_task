from django.urls import path
from positions.views import *

urlpatterns = [
    path('', PositionAPIView.as_view()),
    path('<int:id>/', PositionDetailView.as_view())
]