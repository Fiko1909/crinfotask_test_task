from django.shortcuts import render
from positions.models import Position
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from positions.serializers import PositionListSerializer
# Create your views here.

class PositionAPIView(APIView):
    def get(self, request):
        queryset = Position.objects.all()
        serializer = PositionListSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PositionListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

class PositionDetailView(APIView):
    def get_object(self, id):
        try:
            return Position.objects.get(id=id)
        except Position.DoesNotExist:
            raise Http404

    def delete(self,request,id):
        queryset = self.get_object(id)
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, id):
        queryset = self.get_object(id)
        serializer = PositionListSerializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)