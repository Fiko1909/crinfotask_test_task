from rest_framework import serializers
from positions.models import Position

class PositionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'
