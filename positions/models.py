from django.db import models

class Position(models.Model):
    name = models.CharField(verbose_name='Position name', max_length=64)
    description = models.TextField(verbose_name='Position description', null=True)
