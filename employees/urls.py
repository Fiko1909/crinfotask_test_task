from django.contrib import admin
from django.urls import path
from employees.views import *

urlpatterns = [
    path('', EmployeeAPIView.as_view()),
    path('<int:id>/', EmployeeDetailView.as_view())
]