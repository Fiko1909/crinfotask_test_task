from django.shortcuts import render
from rest_framework.views import APIView
from employees.models import Employee
from employees.serializers import EmployeeListSerializer, EmployeeRelationSerializer
from rest_framework import status
from rest_framework.response import Response
from django.http import Http404
# Create your views here.


class EmployeeAPIView(APIView):

    def get(self,request):
        queryset = Employee.objects.all()
        serializer = EmployeeListSerializer(queryset,many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = EmployeeListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

class EmployeeDetailView(APIView):
    def get_object(self, id):
        try:
            return Employee.objects.get(id=id)
        except Employee.DoesNotExist:
            raise Http404

    def delete(self, request, id):
        queryset = self.get_object(id=id)
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, id):
        queryset = self.get_object(id=id)
        serializer = EmployeeListSerializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)


class EmployeeFullData(APIView):

    def get(self, request):
        queryset = Employee.objects.all()
        serializer = EmployeeRelationSerializer(queryset,many=True)
        return Response(serializer.data)