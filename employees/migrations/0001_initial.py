# Generated by Django 2.2.6 on 2019-10-30 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('persons', '0001_initial'),
        ('positions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField(verbose_name='Start date')),
                ('dismissal_date', models.DateField(null=True, verbose_name='Dismissal date')),
                ('salary', models.IntegerField(verbose_name='Salary')),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.CharField(max_length=64, verbose_name='Created By')),
                ('person_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='persons.Person', verbose_name='Person id')),
                ('position_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='positions.Position', verbose_name='Position id')),
            ],
        ),
    ]
