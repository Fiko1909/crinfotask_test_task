from django.db import models
from persons.models import Person
from positions.models import Position
# Create your models here.


class Employee(models.Model):
    person_id = models.ForeignKey(Person, verbose_name="Person id", on_delete=models.CASCADE)
    position_id = models.ForeignKey(Position, verbose_name="Position id", on_delete=models.CASCADE)
    start_date = models.DateField(verbose_name="Start date")
    dismissal_date = models.DateField(verbose_name="Dismissal date", null=True)
    salary = models.IntegerField('Salary')
    create_date = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField('Created By', max_length=64)